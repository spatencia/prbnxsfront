import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GeneralResponse } from 'src/app/utils/GeneralResponses';
import { Editorial } from 'src/app/models/Editorial/editorial.model';
import { environment } from 'src/environments/environment';
import { EditorialComponent } from 'src/app/components/Editorial/editorial.component';

@Injectable({
  providedIn: 'root'
})
export class EditorialService {

  constructor(private http: HttpClient) { 
  }

  getAll(): Observable<GeneralResponse<Editorial[]>> {
    const path = environment.urlApi + '/Editorial';
    return this.http.get<GeneralResponse<Editorial[]>>(path);
  }

}
