import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GeneralResponse } from 'src/app/utils/GeneralResponses';
import { Autor  } from 'src/app/models/Autor/autor.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AutorService {

  constructor(private http: HttpClient) { 
  }

  getAll(): Observable<GeneralResponse<Autor[]>> {
    const path = environment.urlApi + '/Autor';
    return this.http.get<GeneralResponse<Autor[]>>(path);
  }


}
