import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Editorial } from 'src/app/models/Editorial/editorial.model';
import { EditorialService } from 'src/app/services/Editorial/editorial.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-editorial',
  templateUrl: './editorial.component.html',
  styleUrls: ['./editorial.component.css']
})
export class EditorialComponent implements OnInit {

  constructor(private EdiSer: EditorialService, private _snackBar: MatSnackBar, public dialog: MatDialog) { }

  ELEMENT_DATA: Editorial[] = [];
  dataSource : Editorial[] = [];
  displayedColumns: string[] = ['id','nombre', 'direccionCorrespondencia', 
                                'telefono', 'correoElectronico','maximoLibrosRegistrados'];

  ngOnInit(): void {
    this.consultarEditoriales();
  }

  consultarEditoriales(): void {
    this.EdiSer.getAll().subscribe(x => {
      if (x.estado) {
        this.dataSource = x.data;
      }
      else {
      }
    }, y => {

    });
  }

  addData(): void {

  }

}
