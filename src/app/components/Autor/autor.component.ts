import { Component, OnInit, Inject } from '@angular/core';
import { Autor } from 'src/app/models/Autor/autor.model';
import { AutorService } from 'src/app/services/Autor/autor.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ModalDialog } from 'src/app/components/Autor/modal';

@Component({
  selector: 'app-autor',
  templateUrl: './autor.component.html',
  styleUrls: ['./autor.component.css']
})
export class AutorComponent implements OnInit {

  constructor(private AutSer: AutorService, private _snackBar: MatSnackBar, public dialog: MatDialog) { }

  ELEMENT_DATA: Autor[] = [];
  dataSource : Autor[] = [];
  displayedColumns: string[] = ['id','NombreCompleto', 'FechaNacimiento', 'CiudadProcedencia', 'CorreoElectronico'];

  ngOnInit(): void {
    this.consultarAutores();
  }

  consultarAutores(): void {
    this.AutSer.getAll().subscribe(x => {
     
      if (x.estado) {
        this.dataSource = x.data;
      }
      else {
      }
    }, y => {

    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  addData() { this.openDialog(); }

  removeData() {}

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalDialog, {
      width: '250px',
      data: {name: "Sebastian", animal: "No"}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.animal = result;
    // });
  }

}