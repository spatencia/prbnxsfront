import { Component, OnInit, Inject } from '@angular/core';
import { AutorService } from 'src/app/services/Autor/autor.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'modal-dialog',
    templateUrl: 'modal.html',
  })
  export class ModalDialog {
    
    form:FormGroup;

    constructor(
      public dialogRef: MatDialogRef<ModalDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any, private AutSer: AutorService, fb: FormBuilder) {
  
      this.form = fb.group({
        nombreCompleto: [data.nombreCompleto, Validators.required],
        fechaNacimiento: [data.fechaNacimiento, Validators.required],
        ciudadProcedencia: [data.ciudadProcedencia, Validators.required],
        correoElectronico: [data.correoElectronico, [Validators.email, Validators.required]]
      });
  
      }
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  
    guardar(): void {
      
    }

    getErrorMessage() {
        if (this.form.controls["correoElectronico"].hasError('required')) {
          return 'You must enter a value';
        }
    
        return this.form.controls["correoElectronico"]
                .hasError('email') ? 'Not a valid email' : '';
    }
  
  }