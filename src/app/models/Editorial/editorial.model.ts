export class Editorial {
    private id: number | undefined;
    private Nombre: string | undefined;
    private DireccionCorrespondencia: string | undefined;
    private Telefono: string | undefined;
    private CorreoElectronico: string | undefined;
    private MaximoLibrosRegistrados: number | undefined;
}