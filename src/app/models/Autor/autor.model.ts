export class Autor {
    public Id: number | undefined;
    public NombreCompleto: string | undefined;
    public FechaNacimiento: Date | undefined;
    public CiudadProcedencia: string | undefined;
    public CorreoElectrónico: string | undefined;
}
