import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutorComponent } from './components/Autor/autor.component';
import { EditorialComponent } from './components/Editorial/editorial.component';

const routes: Routes = [
  {path: 'Editorial', component: EditorialComponent  },
  {path: 'Autor', component: AutorComponent  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
